<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Home</a></li>
  <li><a>Login SD/MI</a></li>
  <li><a>Login</a></li>
</ol>
<div class="col-sm-4"></div>
<div class="col-sm-4">
  <div class="panel panel-default">
    <div class="panel-heading">
      <center>Log In SD/MI</center>
    </div>
    <div class="panel-body">
      <div class="form-group">
        <label for="inputNo" class="col-sm-4 control-label">No Ujian</label>
        <div class="col-sm-10">
          <input type="no" class="form-control" id="inputNo" placeholder="No Ujian">
        </div>
        <p>(x-xx-xx-xx-xxx-xxx-x)</p>
        <label for="inputPassword" class="col-sm-4 control-label">Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="inputPassword" placeholder="Password">
        </div>
      </div>
    </div>
    <div class="in">
    <a href="<?= base_url('ppdb/pendaftaran/pendaftaran') ?>"><button type="button" class="btn btn-info">Log In</button></a>
  </div>
  </div>
</div>
<div class="col-sm-4"></div>

<?php endsection() ?>
<?php getview('layouts/layout') ?>