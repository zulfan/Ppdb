<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Statistik</a></li>
  <li><a href="<?= base_url('ppdb/statistiksma') ?>">Pagu SMK</a></li>
</ol>
<table class="table table-bordered table-striped table-hover">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Sekolah</center></td>
    <td><center>Jumlah Pagu</center></td>
  	<td><center>Pagu Terpenuhi</center></td>
  	<td><center>Pagu Tersisa</center></td>
  	<td><center>Bobot Terendah</center></td>
  	<td><center>Bobot Tertinggi</center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>SMKN 1 Boyolangu (Keuangan)</center></td>
  	<td><center>160</center></td>
  	<td><center>146</center></td>
  	<td><center>14</center></td>
  	<td><center>97,10</center></td>
    <td><center>90,20</center></td>
  </tr>
  <tr>
    <td><center>1</center></td>
    <td><center>SMKN 1 Boyolangu (Keuangan)</center></td>
    <td><center>160</center></td>
    <td><center>146</center></td>
    <td><center>14</center></td>
    <td><center>97,10</center></td>
    <td><center>90,20</center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>