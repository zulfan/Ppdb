<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Seleksi</a></li>
  <li><a>Seleksi SMA</a></li>
</ol>
<table class="table table-bordered table-striped table-hover table-responsive">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Sekolah</center></td>
  	<td><center>Pagu Terpenuhi</center></td>
  	<td><center>Pagu Tersisa</center></td>
  	<td><center>Nilai Terendah</center></td>
  	<td><center>Nilai Tertinggi</center></td>
  	<td><center></center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>SMAN 1 Kedungwaru</center></td>
  	<td><center>198</center></td>
  	<td><center>202</center></td>
  	<td><center>37,75</center></td>
  	<td><center>39,50</center></td>
  	<td><center><a class="btn btn-info" href="<?= base_url('ppdb/lihatsma') ?>"><i class="fa fa-eye"></i> Lihat</a></center></td>
  </tr>
  <tr>
  	<td><center>2</center></td>
  	<td><center>SMAN 1 Boyolangu</center></td>
  	<td><center>176</center></td>
  	<td><center>224</center></td>
  	<td><center>34,80</center></td>
  	<td><center>37,80</center></td>
  	<td><center><a class="btn btn-info" href="<?= base_url('ppdb/lihatsma') ?>"><i class="fa fa-eye"></i> Lihat</a></center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>
