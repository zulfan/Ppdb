<?php section('css') ?>
<link href="main.css">
<?php endsection() ?>

<?php section('content') ?>
<ol class="breadcrumb">
  <li><a>Seleksi</a></li>
  <li><a>Seleksi SMP</a></li>
</ol>
<table class="table table-bordered table-striped table-hover table-responsive">
  <tr>
  	<td><center>#</center></td>
  	<td><center>Nama Sekolah</center></td>
  	<td><center>Pagu Terpenuhi</center></td>
  	<td><center>Pagu Tersisa</center></td>
  	<td><center>Nilai Terendah</center></td>
  	<td><center>Nilai Tertinggi</center></td>
  	<td><center></center></td>
  </tr>
  <tr>
  	<td><center>1</center></td>
  	<td><center>SMPN 1 Tulungagung</center></td>
  	<td><center>289</center></td>
  	<td><center>111</center></td>
  	<td><center>28,10</center></td>
  	<td><center>29,85</center></td>
  	<td><center><a class="btn btn-info" href="<?= base_url('ppdb/lihatsmp') ?>"><i class="fa fa-eye"></i> Lihat</a></center></td>
  </tr>
  <tr>
  	<td><center>2</center></td>
  	<td><center>SMPN 1 Tulungagung</center></td>
  	<td><center>289</center></td>
  	<td><center>111</center></td>
  	<td><center>28,10</center></td>
  	<td><center>29,85</center></td>
  	<td><center><a class="btn btn-info" href="<?= base_url('ppdb/lihatsmp') ?>"><i class="fa fa-eye"></i> Lihat</a></center></td>
  </tr>
</table>
<?php endsection() ?>
<?php getview('layouts/layout') ?>