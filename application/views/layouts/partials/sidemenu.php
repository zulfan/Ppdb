<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="left-sidebar">
          <h2>Category</h2>
            <div class="panel-group category-products" id="accordian">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                      <span class="badge pull-right"><i class="fa fa-arrow-down"></i></span>
                      <i class="fa fa-desktop"></i> Desktops
                    </a>
                  </h4>
                </div>
                <div id="sportswear" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="<?= base_url('onshop/menu') ?>">PC </a></li>
                      <li><a href="#">Mac </a></li>
                      <li><a href="#">See All Desktops </a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#mens">
                      <span class="badge pull-right"><i class="fa fa-arrow-down"></i></span>
                      <i class="fa fa-laptop"></i> Laptops & Notebooks
                    </a>
                  </h4>
                </div>
                <div id="mens" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Macs</a></li>
                      <li><a href="#">Windows</a></li>
                      <li><a href="#">See All Laptops & Notebooks</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#womens">
                      <span class="badge pull-right"><i class="fa fa-arrow-down"></i></span>
                      <i class="fa fa-print"></i> Components
                    </a>
                  </h4>
                </div>
                <div id="womens" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Mice and Trackballs</a></li>
                      <li><a href="#">Monitors</a></li>
                      <li><a href="#">Printers</a></li>
                      <li><a href="#">Scanners</a></li>
                      <li><a href="#">Web Cameras</a></li>
                      <li><a href="#">See All Components</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#"><i class="fa fa-tablet"></i> Tablets</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#"><i class="fa fa-windows"></i> Software</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#"><i class="fa fa-mobile"></i> Phones & PDAs</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a href="#"><i class="fa fa-camera"></i> Cameras</a></h4>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#mp3">
                      <span class="badge pull-right"><i class="fa fa-arrow-down"></i></span>
                      <i class="fa fa-headphones"></i> MP3 Players
                    </a>
                  </h4>
                </div>
                <div id="mp3" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Test 3</a></li>
                      <li><a href="#">Test 2</a></li>
                      <li><a href="#">Test 1</a></li>
                      <li><a href="#">See All MP3 Players</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div class="brands_products">
              <h2>Brands</h2>
              <div class="brands-name">
                <ul class="nav nav-pills nav-stacked">
                  <li><a href="<?= base_url('onshop/apple') ?>"> <span class="pull-right">(6)</span>Apple</a></li>
                  <li><a href="<?= base_url('onshop/asus') ?>"> <span class="pull-right">(3)</span>Asus</a></li>
                  <li><a href="<?= base_url('onshop/canon') ?>"> <span class="pull-right">(27)</span>Canon</a></li>
                  <li><a href="<?= base_url('onshop/dell') ?>"> <span class="pull-right">(4)</span>Dell</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Epson</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>HP</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Kensington</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Lenovo</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>LG</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Live Tech</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Macbook</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Microsoft</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(32)</span>Nikon</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(9)</span>Samsung</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Sony</a></li>
                  <li><a href="<?= base_url('onshop/brand') ?>"> <span class="pull-right">(4)</span>Xiaomi</a></li>
                </ul>
              </div>
            </div>
            <br>
            <br>
            <br>
          </div>
        </div>